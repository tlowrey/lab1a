#include <stdio.h>

void intToBin(int, int[8]);
void printBin(int, int[8]);

int main()
{

  int num;
  int binNum[8];

  printf("Please enter a number between 0 and 255:\n");
  scanf("%d", &num);
  if (num < 0 || num > 255)
  {
    while (num < 0 || num > 255)
    {
      printf("Try Again!\n");
      scanf("%d", &num);
    }
  }

  printf("You entered: %d\n", num);

  intToBin(num, binNum);



  return 0;
}


void intToBin(int intNumber, int binaryNumber[8])
{
  int rem = 0;
  int i = 7;
  while (intNumber > 0)
  {
    rem = intNumber % 2;
    intNumber /= 2;
    binaryNumber[i] = rem;
    i--;
  }

  if (i > 0)
  {
    for (; i >= 0; --i)
    {
      binaryNumber[i] = 0;
    }
  }

  printBin(intNumber, binaryNumber);
}

void printBin(int intNumber, int binaryNumber[8])
{
  printf("%d in binary is\n", intNumber);
  for (int i = 0; i < 8; ++i)
  {
    printf("%d", binaryNumber[i]);
  }

  printf("\n");
}
